public class Armstrong{

public static void main(String[] args){

 int number=123, origNum, remainder, result = 0;

    origNum = number;
    while (origNum != 0)
    {
        remainder = origNum%10;
        result += remainder*remainder*remainder;
        origNum /= 10;
    }
    if(result == number)
        System.out.println(number+ "is an Armstrong number.");
    else
        System.out.println(number+ "is not an Armstrong number.");
}
}