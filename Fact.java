public class Fact{

public static void main(String[] args){

   int n=5, i, factorial = 1;
 
    if (n < 0)
      System.out.println("Factorial of a negative number doesn't exist.");
    else
    {
        for(i=1; i<=n; ++i)
        {
            factorial *= i;             
        }
        System.out.println( factorial);
  }}  }